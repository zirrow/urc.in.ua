
<div class="header__currencies" <?php if (count($currencies) < 2) { ?> style="display:none;" <?php } ?> >
	<div class="header__select">
		<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-currency">
			<span class="select select--header select--squer select--transparent">
				<select data-placeholder="" class="select select--header  select--transparent">
				<option>&nbsp;</option>
					<?php foreach ($currencies as $currency) { ?>
						<?php if ($currency['code'] == $code) { ?>
						<option value="<?php echo $currency['code']; ?>" selected="selected"><?php echo $currency['symbol_left']; ?><?php echo $currency['symbol_right']; ?></option>
						<?php } else { ?>
						<option value="<?php echo $currency['code']; ?>"><?php echo $currency['symbol_left']; ?><?php echo $currency['symbol_right']; ?></option>
						<?php } ?>
					<?php } ?>
				
				</select>
			</span>
		<input type="hidden" name="code" value="">
		<input type="hidden" name="redirect" value="<?php echo $redirect; ?>">			
		</form>	
	</div>
</div>
