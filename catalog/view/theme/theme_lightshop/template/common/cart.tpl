			<div class="cart-popup" id="cart">
				<div class="cart-popup__top">
					<span class="cart-popup__close js-cart-close"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg></span>
					<span class="cart-popup__title js-cart-title"><span class="js-cart-items"><?php echo $text_lightshop_cart_title; ?> (<?php echo $text_items; ?>)</span></span>
				</div>
				<div class="cart-popup_bottom js-cart-bottom">
					<div class="scroll-container">
					<?php if ($products || $vouchers) { ?>
						<ul class="cart__list">
						   <?php foreach ($products as $product) { ?>
							<li class="cart__list-item">
								<div class="cart__img">
									<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>"></a>
								</div>
								<div class="cart__descr">
									<span class="cart__article"><?php echo $product['model']; ?></span>
									<a href="<?php echo $product['href']; ?>" class="cart__model"><?php echo $product['name']; ?></a>
									<span class="cart__price"><?php echo $product['total']; ?></span>
									<span class="cart__count"><?php echo $product['quantity']; ?> <?php echo $text_lightshop_cart_quantity; ?></span>
									<?php if ($product['option']) { ?>
									<?php foreach ($product['option'] as $option) { ?>
									<span class="cart__count"><?php echo $option['name']; ?> <?php echo $option['value']; ?></span>
									<?php } ?>
									<?php } ?>
									<?php if ($product['recurring']) { ?>
									<span class="cart__count"><?php echo $text_recurring; ?> <?php echo $product['recurring']; ?></span>
									<?php } ?>
								</div>
								<div class="cart__close">
									<a onclick="cart.remove('<?php echo $product['cart_id']; ?>');" title="<?php echo $button_remove; ?>" class="cart__close-btn"><svg class="icon-cross"><use xlink:href="#cross"></use></svg></a>
								</div>
							</li>
       						   <?php } ?>
							   <?php foreach ($vouchers as $voucher) { ?>
							<li class="cart__list-item">
								<div class="cart__descr">
									<span class="cart__model"><?php echo $voucher['description']; ?></span>
									<span class="cart__price"><?php echo $voucher['amount']; ?></span>
									<span class="cart__count">1 <?php echo $text_lightshop_cart_quantity; ?></span>
								</div>
								<div class="cart__close">
									<a onclick="voucher.remove('<?php echo $voucher['key']; ?>');" title="<?php echo $button_remove; ?>" class="cart__close-btn"><svg class="icon-cross"><use xlink:href="#cross"></use></svg></a>
								</div>
							</li>
							   <?php } ?>
						</ul>
						<?php foreach ($totals as $total) { ?>
						<span class="cart-popup__total"><?php echo $total['title']; ?> <span><?php echo $total['text']; ?></span></span>
						<?php } ?>

						
						
						<div class="cart-popup__btn">

							<a href="<?php echo $cart; ?>" class="btn btn--transparent"><?php echo $text_cart; ?></a>
							<a href="<?php echo $checkout; ?>" class="btn"><?php echo $text_checkout; ?></a>
							<?php if (isset($buy_click['status']) && $buy_click['status']) { ?>
							<div class="cart-popup__link">
								<a href="#popup-buy-click-cc" data-typefrom="cart-popup" data-for="" class="js-fancy-popup-cart"><?php echo $text_lightshop_fast_order; ?></a>
							</div>
							<?php } ?>
						</div>
						<?php } else { ?>
						<span class="cart-popup__discount"><?php echo $text_empty; ?></span>
						<?php } ?>
					</div>
						
					
					
				</div>
				
			</div>
		<?php if (isset($buy_click['status']) && $buy_click['status']) { ?>
		<div class="popup-simple" id="popup-buy-click-cc">
			<div class="popup-simple__inner" >
				<form>
				<h3><?php echo $text_lightshop_fast_order; ?></h3>
					<?php require( DIR_TEMPLATE . 'theme_lightshop/template/product/buyclick_form.tpl' ); ?>
					<?php echo $captcha_fo; ?>
					<button type="button" class="btn js-btn-add-cart-fast-custom quickbuy-send"><?php echo $button_fastorder_sendorder; ?></button>
					<input name="redirect"  value="1" class="fast-redirect" type="hidden">
					<?php if ($text_lightshop_pdata) { ?>
					<div class="popup-simple__inner-personal-data"><?php echo $text_lightshop_pdata; ?></div>
					<?php } ?>
				</form>
			</div>	
		</div>
		<?php } ?>