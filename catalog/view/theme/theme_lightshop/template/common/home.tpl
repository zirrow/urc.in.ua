<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>
	<main class="content">
	<?php if ($column_left || $column_right) { ?>
	<div class="container">
		<div class="row">
			<?php echo $column_left ? '<div class="col-sm-3">' . $column_left . '</div>' : ''?>
			<div class="col-sm-<?php echo $col; ?>">
				<?php echo $content_top; ?>
				<?php echo $content_bottom; ?>
			</div>
			<?php echo $column_right ? '<div class="col-sm-3">' . $column_right . '</div>' : ''?>
		</div>	
	</div>	
	<?php } else { ?>
		<?php echo $content_top; ?>
		<?php echo $content_bottom; ?>
	<?php } ?>
	</main>
<?php echo $footer; ?></body></html>