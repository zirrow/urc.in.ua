</div><!-- FOOTER FIX [END] -->
<!-- FOOTER -->
<?php if ($scroll_to_top) { ?>
	<a href="#" class="scroll-to-top js-stt" style="<?php echo $scroll_to_top_pos; ?>: 0;"><svg class="icon-chevron-top"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#chevron-small-left"></use></svg></a>
<?php } ?>
<?php if ($footer_type == 1) { ?>
<footer class="footer footer--min">
	<div class="footer__top">
		<div class="container">
			<div class="footer__top-inner">
				<div class="subscribe subscribe--dark">
					<?php  if($subscribe_status) { ?>
					<div class="subscribe__heading">
						<span class="subscribe__title">
							<?php echo $subscribe_title;  ?>
						</span>
						<span class="subscribe__subtitle">
							<?php echo $subscribe_subtitle;  ?>
								<?php if ($text_lightshop_pdata) { ?>
									<?php echo $text_lightshop_pdata; ?>
								<?php } ?>
						</span>
					</div>
					<div class="subscribe__form">
						
							<span class="fake-input">
								<input type="email"  name="emailsubscr" value="" placeholder="<?php echo $text_footer_subscribe_email;  ?>">
								<button type="button" class="subscribe__btn" title="<?php echo $text_lightshop_subscribe_btn;  ?>">
								<svg class="icon-plane">
									<use xlink:href="#direction"></use>
								</svg>
								</button>
							</span>
						
					</div>
					<?php } ?>
				</div>
			</div>
		</div>
	</div>
	<div class="footer__bottom">
		<div class="container">
			<div class="footer__bottom-inner">
				<div class="footer__social">
				<?php  if($soc_stat) { ?>
					<?php  if(isset($social_navs)) { ?>
						<ul class="social">
								<?php  foreach ($social_navs as $key => $social_nav) { ?>
									<li class="social__item">
										<a href="<?php echo $social_nav['link']; ?>" target="<?php echo $social_nav['attr']; ?>" class="social__link" rel="nofollow">
												<svg class="icon-fb">
													<use xlink:href="#<?php echo strtolower($social_links[$social_nav['settype']]); ?>"></use>  
												</svg>
										</a>
									</li>								
								<?php } ?>
						</ul>
					  <?php } ?>
				  <?php } ?>
				</div>
				<div class="footer__numbers">
					<div class="footer__numbers-nums">
					<?php if ($phone_1) { ?>
						<a <?php echo 'href="tel:' . preg_replace('/[^0-9]/', '', $phone_1) . '"';?> class="footer__num">
							<?php echo $phone_1; ?>
						</a>
					<?php } ?>
					<?php if ($phone_2) { ?>
						\
						<a <?php echo 'href="tel:' . preg_replace('/[^0-9]/', '', $phone_2) . '"';?> class="footer__num">
							<?php echo $phone_2; ?>
						</a>
					<?php } ?>	
					</div>
					<?php if ($callback_status) { ?>
					<div class="footer__call">
						<a class="btn btn--transparent btn--sm footer__call-btn js-fancy-popup" href="#inline1"><?php echo $text_header_callback; ?></a>
					</div>
					<div id="inline1" style="display:none;"><?php echo $callback; ?></div>
					<?php } ?>	
				</div>
			</div>
		</div>
	</div>
	<span class="body-overlay"></span>
	<span class="body-overlay-filter"></span>
	</footer><!-- FOOTER END -->
<?php } else { ?>	
<footer class="footer">
<?php  if($subscribe_status) { ?>
	<div class="footer__top">
		<div class="container">
			<div class="footer__top-inner">
				<div class="subscribe">
					<div class="subscribe__heading">
						<span class="subscribe__title">
							<?php echo $subscribe_title;  ?>
						</span>
						<span class="subscribe__subtitle">
							<?php echo $subscribe_subtitle;  ?>
								<?php if ($text_lightshop_pdata) { ?>
									<?php echo $text_lightshop_pdata; ?>
								<?php } ?>
						</span>
					</div>
				
					<div class="subscribe__form">
							
							<span class="fake-input">
								<input type="email"  name="emailsubscr" value="" placeholder="<?php echo $text_footer_subscribe_email;  ?>">
								<button type="button" class="subscribe__btn" title="<?php echo $text_lightshop_subscribe_btn;  ?>">
								<svg class="icon-plane">
									<use xlink:href="#direction"></use>
								</svg>
								</button>
							</span>
						
					</div>
				</div>
			</div>
		</div>
	</div>
<?php } ?>
	<div class="footer__bottom">
		<div class="container">
			<div class="footer__bottom-inner">
				<div class="footer__lists">
						<?php  foreach ($footer_navs as $key => $footer_nav) { ?>
						  <div class="footer__lists-item">
							<span class="footer__list-title">
								<?php echo $footer_nav['language'][$language_id]['name']  ?>
							</span>						   

						   <?php if ($footer_nav['settype']) { ?>
								<div><?php echo html_entity_decode($footer_nav['type'][$footer_nav['settype']]['links']['html']); ?></div>
						   <?php } else { ?>
						   <ul class="footer__list">
							   <?php  foreach ($footer_nav['type'][$footer_nav['settype']]['links'] as $id => $link) { ?>
								 <li class="footer__list-item"><a href="<?php echo $link; ?>" class="footer__link" <?php if(isset($top_links[$id]['target'])){ ?>target="<?php echo $top_links[$id]['target']; ?>" <?php } ?>><?php echo isset($top_links[$id]) ? key($top_links[$id]) : ''; ?></a></li>
							   <?php } ?>						   
						   </ul>
						   <?php } ?>
						  </div>
						<?php } ?>				
				
				<?php  if($soc_stat) { ?>
					<?php  if(isset($social_navs)) { ?>
						<div class="footer__lists-item">
							<span class="footer__list-title">
								<?php echo $text_social_navs; ?>
							</span>
							<ul class="footer__list">
								<?php  foreach ($social_navs as $key => $social_nav) { ?>
									<li class="footer__list-item">
										<a href="<?php echo $social_nav['link']; ?>" class="footer__list-icon" rel="nofollow">
											<span class="footer__link-icon">
												<svg class="icon-fb">
													<use xlink:href="#<?php echo strtolower($social_links[$social_nav['settype']]); ?>"></use> 
												</svg>
											</span>
										</a>
										<a href="<?php echo $social_nav['link']; ?>" target="<?php echo $social_nav['attr']; ?>" class="footer__link" rel="nofollow">
											<span class="footer__link-text"><?php echo $social_nav['language'][$language_id]['name']; ?></span>
										</a>
									</li>								
								<?php } ?>
							</ul>
						</div>
					<?php } ?>
				<?php } ?>
				</div>
				<div class="footer__info">					
					<?php if ($footer_t_logo || $footer_logo) { ?>
					<span class="footer__logo">
						<?php if ($footer_logo) { ?>
						<a <?php echo $home == $og_url ? '' : 'href="' . $home .'"' ?> class="footer__logo-link" ><img src="<?php echo $footer_logo; ?>" title="<?php echo $name; ?>" alt="<?php echo $name; ?>"></a>
						<?php } else { ?>
						<a <?php echo $home == $og_url ? '' : 'href="' . $home .'"' ?> class="footer__logo-link"><?php echo $text_logo; ?></a>
						<?php } ?>
					</span>
					<?php } ?>
					<span class="footer__info-text">
						<?php echo $footer_text; ?>
					</span>
					<span class="copyright">
						<?php echo $footer_copyright; ?>
					</span>
				</div>
			</div>
		</div>
	</div>
	<span class="body-overlay">
	</span>
	<span class="body-overlay-filter"></span>
	</footer><!-- FOOTER END -->
<?php } ?>

<?php if ($js_footorhead == 2) { ?>
	<!-- Stylesheets load -->
	<link rel="stylesheet" href="catalog/view/theme/theme_lightshop/css/widgets.css<?php echo '?v'.$version; ?>">
<?php echo $bootstrap_modal ? '<link rel="stylesheet" href="catalog/view/theme/theme_lightshop/css/bootstrap_modal.min.css?v' . $version . '">' : ''; ?>
<?php echo $bootstrap_ttpo ? '<link rel="stylesheet" href="catalog/view/theme/theme_lightshop/css/bootstrap_tooltip_popover.min.css?v' . $version . '">' : ''; ?>
<?php echo $fontawesome ? '<link rel="stylesheet" href="catalog/view/theme/theme_lightshop/css/fa-svg-with-js.css?v' . $version . '">' : ''; ?>
<?php foreach ($styles as $style) { ?>
	<link href="<?php echo $style['href']; ?>" type="text/css" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>">
<?php } ?>	
	<link rel="stylesheet" href="catalog/view/theme/theme_lightshop/css/main.css<?php echo '?v'.$version; ?>">
<?php if ($theme_color && $theme_color < 6) { ?>
	<link rel="stylesheet" href="catalog/view/theme/theme_lightshop/css/color-<?php echo $theme_color; ?>.css<?php echo '?v'.$version; ?>">
<?php } ?>
	<!-- Scripts load -->
	<script src="catalog/view/javascript/theme_lightshop/jquery-2.2.4.min.js<?php echo '?v'.$version; ?>"></script>
	<script src="catalog/view/javascript/theme_lightshop/jquery-ui.min.js<?php echo '?v'.$version; ?>"></script>
	<script src="catalog/view/javascript/theme_lightshop/widgets.js<?php echo '?v'.$version; ?>"></script>
<?php echo $bootstrap_modal ? '<script src="catalog/view/javascript/theme_lightshop/bootstrap_modal.min.js?v' . $version . '"></script>' : ''; ?>
<?php echo $bootstrap_ttpo ? '<script src="catalog/view/javascript/theme_lightshop/bobootstrap_tooltip_popover.min.js?v' . $version . '"></script>' : ''; ?>
<?php echo $fontawesome ? '<script defer src="catalog/view/javascript/theme_lightshop/fontawesome/fontawesome-all.min.js?v' . $version . '"></script>' : ''; ?>
	<script src="catalog/view/javascript/theme_lightshop/functions.js<?php echo '?v'.$version; ?>"></script>
<?php foreach ($scripts as $script) { ?>
	<script src="<?php echo $script; ?>"></script>
<?php } ?>
<?php if ($custom_js) { ?>
	<script><?php echo $custom_js; ?></script>
<?php } ?>
<?php } ?>