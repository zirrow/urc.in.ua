
					<!-- CATALOGUE FULL LIST-->
					<div class="catalogue__full-list js-full-list active">
						<ul class="catalogue__products-full-list">
						<?php foreach ($products as $product) { ?>
							<li class="catalogue__products-full-list-item">
								<div class="products-full-list__img">
								<?php if($product_detail == 1) { ?>
								<!-- Quick view when clicking on a link -->
									<a href="<?php echo $product['href']; ?>" data-for="<?php echo $product['product_id']; ?>" class="products-full-list__img-link js-product-view-btn">
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
									</a>
								<?php } else { ?>
									<a href="<?php echo $product['href']; ?>" class="products-full-list__img-link">
										<img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>">
									</a>
								<?php } ?>
								</div>
								<div class="products-full-list__description">
									<span class="products-full-list__series">
										<?php echo $product['manufacturer']; ?>
									</span>
									<div class="products-full-list__title">
										<span class="products-full-list__name">
										<?php if($product_detail == 1) { ?>
										<!-- Quick view when clicking on a link -->
											<a href="<?php echo $product['href']; ?>" data-for="<?php echo $product['product_id']; ?>" class="catalogue__product-name catalogue__product-name--lg js-product-view-btn">
												<?php echo $product['name']; ?>
											</a>
										<?php } else { ?>
											<a href="<?php echo $product['href']; ?>" class="catalogue__product-name catalogue__product-name--lg">
												<?php echo $product['name']; ?>
											</a>
										<?php } ?>
										</span>
										<?php if ($product['rating'] !== false) { ?>
										<div class="products-full-list__rating">
											<div class="product__rating">										
												<div class="product__rating-fill" style="width: <?php echo $product['rating']*20; ?>%;"></div>
											</div>
										</div>
										<?php } ?>
									</div>
									<div class="products-full-list__notes">
										<span class="products-full-list__status status <?php echo $product['quantity'] > 0 ? 'instock' : ''?>">
											<?php echo $product['stock']; ?>
										</span>
										<span class="products-full-list__compare">
											<a class="products-full-list__compare-link" onclick="compare.add('<?php echo $product['product_id']; ?>');">
												<span class="products-full-list__compare-icon">
												<svg class="icon-add-to-list">
													<use xlink:href="#add-to-list"></use>
												</svg>
												</span>
												<span class="products-full-list__compare-text">
													<?php echo $button_compare; ?>
												</span>
											</a>
										</span>
									</div>
									<p class="products-full-list__text">
										<?php echo $product['description']; ?>
									</p>
									<div class="products-full-list__description-bottom products-qty-info">
										<div class="products-full-list__price">
										<?php if ($product['price']) { ?>
											<?php if ($product['special']) { ?>
												<span class="catalogue__price-old catalogue__price-old--lg">
													<?php echo $product['price']; ?>
												</span>
												<span class="catalogue__price catalogue__price--lg">
													<?php echo $product['special']; ?>
												</span>
											<?php } else { ?>	
												<span class="catalogue__price catalogue__price--lg">
													<?php echo $product['price']; ?>
												</span>											
											<?php } ?>	
										<?php } ?>	
										</div>
										<div class="products-full-list__action">
											<span class="products-full-list__action-btn">
												<button type="button" class="btn products-full-list__btn catalogue__btn-cart-list" data-for="<?php echo $product['product_id']; ?>" ><?php echo $button_cart; ?></button>
											</span>
											<?php if ($buy_click['status']) { ?>
											<a href="#popup-buy-click" data-for="<?php echo $product['product_id']; ?>" data-typefrom="category-list" class="products-full-list__action-link js-fancy-popup-cart"><?php echo $text_lightshop_buy_click; ?></a>
											<?php } ?>
										</div>
										<div class="products-full-list__spinner products-qty-info-spinner">
											<div class="spinner-wrap">
												<input type="text" class="spinner" name="quantity" value='<?php echo $product['minimum']; ?>' placeholder="<?php echo $product['minimum']; ?>">
											</div>
										</div>
									</div>
								</div>
							</li>
						<?php } ?>
						</ul>
					</div>
					<!-- CATALOGUE FULL LIST END-->
