<?php if ($reviews) { ?>
<?php foreach ($reviews as $review) { ?>
							<div <?php echo $schema ? 'itemprop="review" itemscope itemtype="http://schema.org/Review"' : ''?> class="product-info__feedback-item">
								<div class="product-info__feedback-img">
									<svg class="icon-user">
										<use xlink:href="#user"></use>
									</svg>
								</div>
								<div class="product-info__feedback-title">
									<span <?php echo $schema ? 'itemprop="author"' : ''?>><?php echo $review['author']; ?></span>
									<span class="product-info__feedback-time" <?php echo $schema ? 'itemprop="datePublished"' : ''?> content="<?php echo $review['date_added_schema']; ?>">
										<svg class="icon-calendar">
											<use xlink:href="#calendar"></use>
										</svg>
										<?php echo $review['date_added']; ?>
									</span>
								</div>
								<div class="product-info__feedback-rating">
									<div class="product__rating">
										<div class="product__rating-fill" style="width: <?php echo 20*$review['rating']; ?>%;"  >	
										</div>
									</div>
								</div>
								<p <?php echo $schema ? 'itemprop="description"' : ''?>class="product-info__feedback-text"><?php echo $review['text']; ?></p>
							</div>
<?php } ?>
<?php echo $pagination; ?>
<?php } else { ?>
<p class="product-info__feedback-noreviews"><?php echo $text_no_reviews; ?></p>
<?php } ?>