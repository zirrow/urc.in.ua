<?php echo $header; $col = $column_left ? 9 : 12; $col = $column_right ? $col - 3 : $col; ?>

<?php if ($products) { /* Categories with products */  ?>
<!--MAIN CONTENT CATALOGUE-->
<main <?php echo $schema ? 'itemscope itemtype="http://schema.org/Product"' : ''?> class="content <?php echo $thumb && $category_background ? 'content-catalogue' : ''?>">

<?php if ($content_top) { ?>
	<div class="container">
	<?php echo $content_top; ?>
	</div>
<?php } ?>

	<?php if ($schema) { ?>
		<!-- Microdata -->
		<div itemtype="http://schema.org/AggregateOffer" itemscope="" itemprop="offers">
			<meta content="<?php echo $product_total; ?>" itemprop="offerCount"> <!-- Number of goods -->
			<meta content="<?php echo (int)$maxPrice; ?>" itemprop="highPrice"> <!-- The highest price for a product -->
			<meta content="<?php echo (int)$minPrice; ?>" itemprop="lowPrice"> <!-- The lowest price for the goods -->
			<meta content="<?php echo $currency; ?>" itemprop="priceCurrency"> <!-- Currency -->
		</div>
	<?php } ?>

<div class="catalogue">
	
	
	<!-- CATALOGUE TOP-->
	<?php if ($thumb && $category_background) { ?>
	<div class="catalogue__top" style="background-image: url(<?php echo $thumb; ?>);">
	<?php } ?>	
		<div class="container">
			<div class="breadcrumbs <?php echo $thumb && $category_background ? 'catalogue__breadcrumbs' : 'breadcrumbs--sm-pad'?>">  
				<ul class="breadcrumb__list">
				<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
				<?php if($i == 0) { ?>
					<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>
				<?php } elseif($i + 1 < count($breadcrumbs)) { ?>
					<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><a data-href="#categories-popup<?php echo $i; ?>" class="js-popup-call-hover breadload" data-id="<?php echo $breadcrumb['cat_id']; ?>" data-i="<?php echo $i; ?>" href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>		
				<?php } else { ?>
					<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><?php echo $breadcrumb['text']; ?></li>		
				<?php } ?>
				<?php } ?> 			
				</ul>
				<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
					<div class="popup" id="categories-popup<?php echo $i; ?>">
					</div>
				<?php } ?> 	
			</div>
	
				
			<?php if ($thumb && $category_background) { ?>
			<h1 <?php echo $schema ? 'itemprop="name"' : ''?> class="catalogue__title"><?php echo $heading_title; ?></h1>
			<?php } else { ?>
			<h1 <?php echo $schema ? 'itemprop="name"' : ''?> class="content__title"><?php echo $heading_title; ?></h1>
			<?php } ?>
		</div>
	<?php if ($thumb && $category_background) { ?>	
	</div>
	<?php } ?>	
	<!-- CATALOGUE TOP-->
	
	<!-- CATALOGUE MAIN CONTENT INNER-->
	<div class="catalogue__inner" <?php echo $thumb && $category_background ? '' : 'style="padding-top: 0;"'?>>
		<div class="container">

<?php } else { /* Categories without products */ ?>	
		
<!--MAIN CONTENT CATEGORIES-->
<main class="content">
	<div class="container">
	<?php echo $content_top; ?>

		<div class="breadcrumbs breadcrumbs--sm-pad">
			<ul class="breadcrumb__list">
			<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
			<?php if($i == 0) { ?>
				<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><a href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>
			<?php } elseif($i + 1 < count($breadcrumbs)) { ?>
				<li <?php echo $schema ? 'itemscope itemtype="http://data-vocabulary.org/Breadcrumb"' : ''?> class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><a data-href="#categories-popup<?php echo $i; ?>" class="js-popup-call-hover" href="<?php echo $breadcrumb['href']; ?>" <?php echo $schema ? 'itemprop="url"' : ''?>><span <?php echo $schema ? 'itemprop="title"' : ''?>><?php echo $breadcrumb['text']; ?></span></a></li>		
			<?php } else { ?>
				<li class="breadcrumb__list-item"><svg class="icon-chevron-right"><use xlink:href="#chevron-small-left"></use></svg><?php echo $breadcrumb['text']; ?></li>		
			<?php } ?>
			<?php } ?> 			
			</ul>
				<?php foreach ($breadcrumbs as $i=> $breadcrumb) { ?>
					<div class="popup" id="categories-popup<?php echo $i; ?>">
						<div class="nav-submenu nav-submenu--min">
							<ul class="nav-submenu__list">
					<?php if(isset($breadcrumb['breadList'])) { ?>
						<?php foreach ($breadcrumb['breadList'] as $breadlist) { ?>
							<li class="nav-submenu__list-item"><a href="<?php echo $breadlist['href']; ?>" class="nav-submenu__link"><?php echo $breadlist['name']; ?></a></li>
						<?php }  ?>
					<?php }  ?>
							</ul>
						</div>
					</div>
				<?php } ?> 	
		</div>
			
<?php } ?>		

<input type="hidden" id="path_id" value='<?php echo $path; ?>'>
<input type="hidden" id="url" value='<?php echo $url; ?>'>

<?php if ($categories) { ?>
	<div class="categories">

		<div class="categories__inner">

				<?php if (!$products && $category_categories) { ?>
				<div class="categories__heading categories__heading--col-<?php echo $category_categories; ?> <?php echo count($categories) == 1 ? 'categories-heading--fix' : ''?>">
					<h1 class="categories__title js-cat-title-height">
					<?php echo $heading_title; ?>
					</h1>
					<?php if ($description) { ?>
					<div class="categories__heading-text">
						<div class="categories__heading-text-inner js-custom-scroll">					
							<div class="editor">
								<?php echo $description; ?>		
							</div>
						</div>
					</div>
					<?php } ?>
				</div>
				<?php } ?>
	
			
			<div class="row">
				<?php if ($category_categories) { ?>
					<?php foreach ($categories as $i=> $category) { ?>
					<?php if($i < ($category_categories == 2 ? 3 : 2)) { ?>
					<div class="col-md-<?php echo $category_categories; ?> col-sm-6">
						<div class="categories__item categories__item--col-<?php echo $category_categories; ?>">
							<a href="<?php echo $category['href']; ?>" class="categories__link js-animate" style="background-image: url(<?php echo $category['thumb']; ?>); <?php echo $category_background ? '' : 'background-size: contain;'?>">
								<div class="categories__caption js-animate-caption">
									<span class="categories__caption-text">
										<?php echo $category['name']; ?>
									</span>
								</div>
								<span class="categories-overlay"></span>
							</a>
						</div>
					</div>
					<?php } ?>
					<?php } ?>

					<?php if (!$products) { ?>
					<div class="<?php echo count($categories) == 1 ? 'col-md-8' : 'col-md-' . ($category_categories == 4 ? 4 : 6) ?>  col-sm-6 col-xs-6 categories__item-heading">
						<div class="categories__item categories__item--col-<?php echo $category_categories; ?>"></div>
					</div>
					<?php } ?>

					<?php foreach ($categories as $i=> $category) { ?>
					<?php if($i >= ($category_categories == 2 ? 3 : 2)) { ?>
					<div class="col-md-<?php echo $category_categories; ?> col-sm-6">
						<div class="categories__item categories__item--col-<?php echo $category_categories; ?>">
							<a href="<?php echo $category['href']; ?>" class="categories__link js-animate" style="background-image: url(<?php echo $category['thumb']; ?>); <?php echo $category_background ? '' : 'background-size: contain;'?>">
								<div class="categories__caption js-animate-caption">
									<span class="categories__caption-text">
										<?php echo $category['name']; ?>
									</span>
								</div>
								<span class="categories-overlay"></span>
							</a>
						</div>
					</div>				
					<?php } ?>
					<?php } ?>
				<?php } ?>
			</div>
			
			
		</div>
		
	</div>
<?php } ?>

<div class="clearfix"></div>
	<div class="row">
	
<?php if ($products) { ?>		
		<?php echo $column_left ? '<div class="col-md-3"><div class="catalogue__sidebar">' . $column_left . '</div></div>' : ''?>
		<div class="col-md-<?php echo $col; ?>">
		<?php echo $column_right ? '<div id="sidebar-mob-btn"></div>' : ''?>

<div class="clearfix"></div>

			<!-- CATALOGUE CONTENT-->		
			

			<div class="catalogue__content">
				<div class="catalogue__content-top">
					<div class="catalogue__sort">
						<?php if ($category_sorts) { ?>
						<div class="catalogue__sort-sorts">
							<span class="select select--squer select--transparent">
								<select data-placeholder="<?php echo $text_sort; ?>" class="select select--transparent"  onchange="location = this.value;">
								<option><?php echo $text_sort; ?></option>
									<?php foreach ($sorts as $sorts) { ?>
										<?php if ($sorts['value'] == $sort . '-' . $order) { ?>
										<option value="<?php echo $sorts['href']; ?>" selected="selected"><?php echo $text_sort; ?> <?php echo $sorts['text']; ?></option>
										<?php } else { ?>
										<option value="<?php echo $sorts['href']; ?>"><?php echo $text_sort; ?> <?php echo $sorts['text']; ?></option>
										<?php } ?>
									<?php } ?>
								
								</select>
							</span>
						</div>
						<?php } ?>
						<?php if ($category_limits) { ?>
						<div class="catalogue__sort-limits">
							<span class="select select--squer select--transparent">
								<select data-placeholder="<?php echo $text_limit; ?>" class="select select--transparent"  onchange="location = this.value;">
								<option><?php echo $text_limit; ?></option>
								<?php foreach ($limits as $limits) { ?>
								<?php if ($limits['value'] == $limit) { ?>
								<option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $text_limit; ?> <?php echo $limits['text']; ?></option>
								<?php } else { ?>
								<option value="<?php echo $limits['href']; ?>"><?php echo $text_limit; ?> <?php echo $limits['text']; ?></option>
								<?php } ?>
								<?php } ?>
								</select>
							</span>
						</div>
						<?php } ?>
					</div>

					<div class="catalogue__product-view">
						<div class="product-view">
							<ul class="product-view__list js-drop-select-drop">
								<li class="product-view__list-item active js-list-select js-select-lists" data-columns="main">
									<a href="#" class="product-view__link">
										<svg class="icon-tile-three">
											<use xlink:href="#tile-three"></use>
										</svg>
									</a>
								</li>
								<li class="product-view__list-item  js-list-select js-select-table" data-columns="table">
									<a href="#" class="product-view__link">
										<svg class="icon-table">
											<use xlink:href="#table"></use>
										</svg>
									</a>
								</li>
								<li class="product-view__list-item  js-list-select js-select-full-list" data-columns="list"> 
									<a href="#" class="product-view__link">
										<svg class="icon-tile-wide">
											<use xlink:href="#tile-wide"></use>
										</svg>
									</a>
								</li>
								<li class="product-view__list-item js-list-select js-select-four-items product-view__list-item--hide-mobile" data-columns="main-four">
									<a href="#" class="product-view__link">
										<svg class="icon-tile-four">
											<use xlink:href="#tile-four"></use>
										</svg>
									</a>
								</li>
								<li class="product-view__list-item js-list-select js-select-five-items product-view__list-item--hide-mobile" data-columns="main-five">
									<a href="#" class="product-view__link">
										<svg class="icon-tile-five">
											<use xlink:href="#tile-five"></use>
										</svg>
									</a>
								</li>
							</ul>
						</div>
					</div>
				</div>
				<!-- CATALOGUE CONTENT (LISTS)-->
				<div  id="mainContainer" class="catalogue__content-lists">
					<?php require( DIR_TEMPLATE . 'theme_lightshop/template/product/category_'.$view.'.tpl' ); ?>

					<!-- CATALOGUE LISTS END-->
				</div>
				<!-- CATALOGUE CONTENT (LISTS) END-->
				
				<!-- CATALOGUE PAGINATION-->
				<div class="catalogue__content-bottom">
					<div class="catalogue__pagination">
						<?php echo $pagination; ?>
					</div>
				</div>
				<!-- CATALOGUE PAGINATION END-->
			</div>
			


	<?php if ($description) { ?>
		<div class="catalogue__description"><div <?php echo $schema ? 'itemprop="description"' : ''?> class="editor"><?php echo $description; ?></div></div>
	<?php } else { ?>
	<?php echo $schema ? '<meta itemprop="description" content="' . $heading_title . '">' : ''?>
	<?php } ?>
<?php } ?>

<?php if ((!$categories || $category_categories == 0) && !$products){ ?>
<div class="col-md-12">
	<h1 class="categories__title">
	<?php echo $heading_title; ?>
	</h1>
	<?php if ($description) { ?>					
		<div class="catalogue__description"><div class="editor"><?php echo $description; ?></div></div>	
	<?php } ?>	
	<p><?php echo $text_empty; ?></p>
	<div class="checkout__button-box">
	<a href="<?php echo $continue; ?>" class="btn btn--transparent"><?php echo $button_continue; ?><svg class="icon-arrow-long-right"><use xmlns:xlink="http://www.w3.org/1999/xlink" xlink:href="#arrow-long-right"></use></svg></a>
	</div>
</div>
<?php } ?>

<?php if ($products) { ?>
		</div>
	<?php echo $column_right ? '<div class="col-md-3"><div class="sidebar sidebar--right">' . $column_right . '</div></div>' : ''?>
<?php } ?>

	</div>	



<?php if ($products) { ?>
			<!-- CATALOGUE CONTENT END-->
		</div><!-- container end -->
	</div><!-- catalogue__inner end -->
	<!-- CATALOGUE MAIN CONTENT INNER END-->
</div><!-- catalogue end -->
<?php } else { /* Categories without products */ ?>
	<?php echo $content_bottom; ?>
	</div><!-- container end -->
<?php } ?>

<input type="hidden" id="path" value='<?php echo $path; ?>'>
<input type="hidden" id="cat_url" value='<?php echo $cat_url; ?>'>
<input type="hidden" id="currencydata" value='<?php echo $currencydata; ?>'>
<div id="popupprod" class="catalogue__product-detail js-tabs-box js-product-view">
</div>

</main>
		<?php if ($buy_click['status']) { ?>
		<div class="popup-simple" id="popup-buy-click">
			<div class="popup-simple__inner" >
				<form>
				<h3><?php echo $text_lightshop_buy_click; ?></h3>
					<?php require( DIR_TEMPLATE . 'theme_lightshop/template/product/buyclick_form.tpl' ); ?>
					<?php echo $captcha_fo; ?>
					<button type="button" class="btn js-btn-add-cart-fast-popup quickbuy-send"><?php echo $button_fastorder_sendorder; ?></button>
					<input name="quantity"  value="" id="cat_qty" type="hidden">
					<input name="product_id" value="" id="cat_prod_id" type="hidden">
					<input name="redirect"  value="1" class="fast-redirect" type="hidden">
					<?php if ($text_lightshop_pdata) { ?>
					<div class="popup-simple__inner-personal-data"><?php echo $text_lightshop_pdata; ?></div>
					<?php } ?>
				</form>
			</div>	
		</div>
		<?php } ?>
<?php echo $footer; ?>
<script type="text/javascript"><!--
$(document).on('ready', function() {
	var view = '<?php echo $viewLayer; ?>';
	if(window.innerWidth < 767) {
		view = '<?php echo $viewLayerM; ?>';
		$('.js-lists').removeClass('catalogue__products-list--four');
		$('.js-lists').removeClass('catalogue__products-list--five');
	}else{
		var v  = '<?php echo $viewSub; ?>';
		$('.js-lists').removeClass('catalogue__products-list--four');
		$('.js-lists').removeClass('catalogue__products-list--five');
		if(v != ''){
			$('.js-lists').addClass('active ' + v);
		}		
	}
	
	if (localStorage.getItem('display')) {
//		view = localStorage.getItem('display');
	}

	$(".product-view__list-item").removeClass('active'); 
	$(".product-view__list-item").each(function(indx, element){
		if($(element).attr('data-columns') == view){
			$(element).addClass('active');
		}		
	});
	$('.product-view__list .active').insertBefore($(".product-view__list-item").first());

	breadLoad();	
	
});
//--></script>
</body></html>
